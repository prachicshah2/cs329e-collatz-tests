#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import TestCase, main

import Collatz as ctz

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):

    # ----
    # read
    # ----

    def test_read(self):
        s = '1 10\n'
        i, j = ctz.collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_non_int(self):
        # This function should fail when any value isn't an integer literal.
        s0 = '1.5 10\n'
        with self.assertRaises(ValueError):
            ctz.collatz_read(s0)

        s1 = 'a_string 10\n'
        with self.assertRaises(ValueError):
            ctz.collatz_read(s1)

    def test_read_maintains_order(self):
        # Input order should not be permuted by this function.
        s = '10 1\n'
        i, j = ctz.collatz_read(s)
        self.assertEqual(i, 10)
        self.assertEqual(j, 1)

    # ----------
    # safe_range
    # ----------

    def test_safe_range_same(self):
        range_vals = list(ctz.safe_range(0, 1))
        self.assertEqual(range_vals[0], 0)
        self.assertEqual(range_vals[-1], 1)

    def test_safe_range_wrong_order(self):
        range_vals = list(ctz.safe_range(1, 0))
        self.assertEqual(range_vals[0], 0)
        self.assertEqual(range_vals[-1], 1)

    def test_safe_range_construction_basic(self):
        range_0 = ctz.safe_range(1, 0)
        range_1 = ctz.safe_range(0, 1)
        self.assertEqual(range_0, range_1)

    def test_safe_range_construction_negative(self):
        range_0 = ctz.safe_range(-1, -10)
        range_1 = ctz.safe_range(-10, -1)
        self.assertEqual(range_0, range_1)

    # -----------------
    # collatz_stop_len
    # -----------------

    def test_len_1(self):
        v = ctz.collatz_stop_len(1)
        self.assertEqual(v, 1)

    def test_len_2(self):
        v = ctz.collatz_stop_len(2)
        self.assertEqual(v, 2)

    def test_len_3(self):
        v = ctz.collatz_stop_len(3)
        self.assertEqual(v, 8)

    def test_len_4(self):
        v = ctz.collatz_stop_len(4)
        self.assertEqual(v, 3)

    def test_len_non_positive(self):
        with self.assertRaises(ValueError):
            ctz.collatz_stop_len(0)

        with self.assertRaises(ValueError):
            ctz.collatz_stop_len(-1)

    def test_len_non_int(self):
        with self.assertRaises(ValueError):
            ctz.collatz_stop_len(0.5)

    def test_len_999999(self):
        v = ctz.collatz_stop_len(999999)
        self.assertEqual(v, 259)

    # -----
    # cache
    # -----

    def test_cache_same(self):

        def f(n):
            if 0 <= n < 2:
                return n

            return f(n - 1) + f(n - 2)

        test_val = 21  # Only odd values are cached by simple_cache.
        non_cached = f(test_val)
        cached = ctz.simple_cache(f)(test_val)
        self.assertEqual(non_cached, cached)

    def test_cache_wrapped(self):
        # Shows that function wrapping was done correctly.
        # Shows that use of cached func doesn't mutate underlying function.
        def test_func(x): return x
        wrapped_func = ctz.simple_cache(test_func)

        test_val = 7  # Only odd values are cached by simple_cache.
        self.assertEqual(test_func(test_val), wrapped_func(test_val))
        self.assertIs(test_func, wrapped_func.__wrapped__)
        self.assertEqual(test_func(test_val),
                         wrapped_func.__wrapped__(test_val))

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = ctz.collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = ctz.collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = ctz.collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = ctz.collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_reversed(self):
        v = ctz.collatz_eval(10, 1)
        self.assertEqual(v, 20)

    def test_eval_base_single(self):
        # Single-valued range for the base-case.
        v = ctz.collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_nonbase_single(self):
        # Single-valued range containing a non-base case.
        v = ctz.collatz_eval(10, 10)
        self.assertEqual(v, 7)

    def test_eval_corner(self):
        # Corner case, entire range based on specification.
        v = ctz.collatz_eval(1, 999999)
        self.assertEqual(v, 525)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        ctz.collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), '1 10 20\n')

    def test_print_reversed(self):
        # Make sure input order is preserved.
        w = StringIO()
        ctz.collatz_print(w, 10, 1, 20)
        self.assertEqual(w.getvalue(), '10 1 20\n')

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO('1 10\n100 200\n201 210\n900 1000\n')
        w = StringIO()
        ctz.collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), '1 10 20\n100 200 125\n201 210 89\n900 1000 174\n')

    def test_solve_empty(self):
        r = StringIO('')
        w = StringIO()
        ctz.collatz_solve(r, w)
        self.assertEqual(w.getvalue(), '')

    def test_solve_extra_newline(self):
        # Expect a truncated output due to extra newline after `100 200`.
        r = StringIO('1 10\n100 200\n\n201 210\n900 1000\n')
        w = StringIO()
        ctz.collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), '1 10 20\n100 200 125\n')

    def test_solve_extra_newline_end(self):
        r = StringIO('1 10\n100 200\n201 210\n900 1000\n\n\n')
        w = StringIO()
        ctz.collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), '1 10 20\n100 200 125\n201 210 89\n900 1000 174\n')


# ----
# main
# ----
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
